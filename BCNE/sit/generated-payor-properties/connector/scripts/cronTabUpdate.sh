#!/bin/bash
set -e
 
(crontab -l ; echo "*/5 * * * * /home/cprime/cprime/healthrules-connector-server-4.0.4/bcbsne-job-monitoring-report/resources/script/sendEmail.sh >> /home/cprime/bcbsne-job-monitoring-report/cronLog.log 2>&1") | sort - | uniq - | crontab -

cd /home/cprime/cprime/healthrules-connector-server-4.0.4/bcbsne-job-monitoring-report/resources/script/
chmod 777 sendEmail.sh