#!/bin/bash
set -e

HE_DIR=$1
HOST_NAME=$2


echo ""
echo "********** BCBSNE SH : bcbsne_hearbeatid_wss.sh.sh start on ${HOST_NAME} **********"

wssKeyFile=$(grep "org.apache.ws.security.crypto.merlin.file" $HE_DIR/etc/com.healthedge.customer.bcbsne.heartbeatid.wssecurity.cfg | sed -e "s/org.apache.ws.security.crypto.merlin.file=//g")
echo "key file dir $wssKeyFile"
wssKeyDir=$(dirname "${wssKeyFile}")
        mkdir -p $HE_DIR/$wssKeyDir
		cp HeartbeatProxyMessageEncryptTs1.pfx $HE_DIR/$wssKeyFile
		echo "copied to $HE_DIR/$wssKeyFile"

        chmod -R 755 $HE_DIR/$wssKeyFile

echo "********** BCBSNE SH : bcbsne_hearbeatid_wss.sh.sh end on ${HOST_NAME} **********"
echo ""
